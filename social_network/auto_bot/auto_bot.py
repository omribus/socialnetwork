from faker import Faker
from random import randint, choice
import requests
import json

faker = Faker()


class BotApi:
    def __init__(self, server_config: dict, bot_config: dict):
        self.base_url = f"http://{server_config['server_host']}:{server_config['server_port']}"
        self.config = bot_config

    def _create_full_url(self, api_endpoint: str) -> str:
        return f"{self.base_url}/api/{api_endpoint}"

    def signup(self, username: str, password: str, email: str) -> requests.Response:
        return requests.post(url=self._create_full_url("signup"),
                             data={"name": username, "password": password, "email": email})

    def login(self, username: str, password: str) -> requests.Response:
        return requests.post(url=self._create_full_url("login"),
                             data={"username": username, "password": password})

    def like(self, post_id: int, token: str) -> requests.Response:
        return requests.post(url=self._create_full_url("feed"),
                             data={"post_id": post_id, "action": "like"},
                             headers={"JWT": token})

    def unlike(self, post_id: int, token: str) -> requests.Response:
        return requests.post(url=self._create_full_url("feed"),
                             data={"post_id": post_id, "action": "unlike"},
                             headers={"JWT": token})

    def post(self, content: str, token: str) -> requests.Response:
        return requests.post(url=self._create_full_url("new_post"),
                             data={"content": content},
                             headers={"JWT": token})

    def get_likes_info(self) -> requests.Response:
        """ Get from the server who the next player to perform a 'like' is,
        and what are the possible posts for him to like """
        return requests.post(url=self._create_full_url("likes_info"),
                             data={"max_likes_per_user": self.config['max_likes_per_user']})


class AutoBot:
    def __init__(self):
        with open('server/config.json') as server_config_file:
            server_config = json.loads(server_config_file.read())

        with open('auto_bot/config.json') as config_file:
            self.config = json.loads(config_file.read())

        self.api = BotApi(server_config, self.config)
        self.users_credentials = dict()

    def _write_to_log(self, entry: str):
        with open(self.config["log_path"], 'a+') as log_file:
            log_file.write(f"{entry}\n\n")

    def _signup_users(self):
        for _ in range(self.config["number_of_users"]):
            random_username, random_password = faker.first_name(), faker.password()
            user_email = f"{random_username}@gmail.com"

            response = self.api.signup(random_username, random_password, user_email).json()
            self._write_to_log(response)

            self.users_credentials[random_username] = random_password

    def _create_random_posts(self):
        for username in self.users_credentials:
            num_of_posts = randint(0, self.config["max_posts_per_user"])

            for _ in range(num_of_posts):
                token = self.api.login(username, self.users_credentials[username]).json()["token"]
                result = self.api.post(content=faker.sentence(), token=token).json()
                self._write_to_log(result)

    def _give_likes(self):
        while True:
            response = self.api.get_likes_info().json()
            if not response["possible_posts"]:
                break

            next_user, possible_posts_ids = response["user"], response["possible_posts"]
            random_post_id_to_like = choice(possible_posts_ids)

            token = self.api.login(next_user, self.users_credentials[next_user]).json()["token"]
            like_response = self.api.like(random_post_id_to_like, token).json()
            self._write_to_log(like_response)

    def run(self):
        try:
            self._signup_users()
            self._create_random_posts()
            self._give_likes()

        except Exception as err:
            self._write_to_log(str(err))


if __name__ == '__main__':
    AutoBot().run()
