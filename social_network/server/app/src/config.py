class Config:
    LOG_FILE_PATH = "server/log.txt"

    MAX_LENGTH_USER_NAME = 100
    MAX_LENGTH_USER_PASSWORD = 100

    JWT_SECRET = 'AVeryComplicatedJSONWebTokenEncryptionSecretKey'
    JWT_ALGORITHM = 'HS256'
    JWT_EXP_SECONDS = 300

    HUNTER_API_KEY = "1c9d18dfb25a6f2882b44b17aedc6baca47a7f44"
    CLEARBIT_API_KEY = "sk_c5333b0734ac689e9b758f995fbdc805"
