from app.src.config import Config


class Logger:
    @staticmethod
    def log(entry: str):
        with open(Config.LOG_FILE_PATH, 'a+') as log_file:
            log_file.write(f"{entry}\n\n")
