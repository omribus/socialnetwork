# Generated by Django 3.1.4 on 2020-12-26 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('src', '0003_user_additional_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='additional_data',
            field=models.JSONField(default={}),
        ),
    ]
