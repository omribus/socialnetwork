from django.db import models
from app.src.config import Config


class User(models.Model):
    name = models.CharField(max_length=Config.MAX_LENGTH_USER_NAME, unique=True)
    password = models.CharField(max_length=Config.MAX_LENGTH_USER_PASSWORD)
    email = models.EmailField(unique=True)
    additional_data = models.JSONField(default=dict)

    def __str__(self):
        return self.name


class Post(models.Model):
    content = models.TextField()
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="posts")
    liked_by = models.ManyToManyField(User, related_name="likes", blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return f"{self.id} - '{self.content}' ({self.posted_by}, {self.liked_by.count()} likes)"
