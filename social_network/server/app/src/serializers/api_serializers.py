from rest_framework import serializers


class CreateNewPostSerializer(serializers.Serializer):
    content = serializers.CharField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class FeedSerializer(serializers.Serializer):
    post_id = serializers.IntegerField(min_value=1, label="Post ID", required=True)
    action = serializers.ChoiceField([('like', 'like'), ('unlike', 'unlike')], label="Like/Unlike")

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class LikesInfoSerializer(serializers.Serializer):
    max_likes_per_user = serializers.IntegerField(min_value=1)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
