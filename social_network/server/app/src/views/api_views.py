from rest_framework.views import APIView
from rest_framework.response import Response

from app.src import authentication
from app.src.models import Post, User
from app.src.serializers.api_serializers import *
from app.src.serializers.admin_serializers import UserSerializer
from app.src.enricher import get_additional_data


class CreateNewUserView(APIView):
    serializer_class = UserSerializer

    def post(self, request):
        """ Signup a new User """
        serializer_result = self.serializer_class(data=request.data)

        if not serializer_result.is_valid():
            return Response(serializer_result.errors)

        if authentication.verify_email_hunter(request.data["email"]) == "invalid":
            return Response("Email address is not valid")

        user_info = {key: request.data[key] for key in ["name", "password", "email"]}
        user_info["additional_data"] = get_additional_data(request.data["email"])

        new_user = User(**user_info)
        new_user.save()
        return Response(f"Welcome {new_user.name}! Your ID is: {new_user.id}!")


class LoginView(APIView):
    serializer_class = LoginSerializer

    def post(self, request):
        """ If valid credentials were entered, a JTW is sent back """
        try:
            user = authentication.verify_user_pass(request.data.get('username'), request.data.get('password'))
        except Exception as err:
            return Response("No User was found with the provided credentials.")

        jwt_token = authentication.create_jwt_token(user)
        return Response({"token": jwt_token})


class FeedView(APIView):
    serializer_class = FeedSerializer

    @authentication.requires_token
    def get(self, request, user_id=None):
        """ Show all posts by users """
        return [str(post) for post in Post.objects.all()]

    @authentication.requires_token
    def post(self, request, user_id=None):
        """ Like or unlike an existing post """
        serializer_result = FeedSerializer(data=request.data)
        if not serializer_result.is_valid():
            return Response(serializer_result.errors)

        post_id, action_type = request.data.get("post_id"), request.data.get('action')
        user, post = User.objects.get(id=user_id), Post.objects.get(id=post_id)

        if post.posted_by == user:
            return Response(f"You cannot {action_type} your own post!")

        post.liked_by.add(user) if action_type == "like" else post.liked_by.remove(user)
        return Response(f"Post with ID {post_id} was {action_type}d by {user.name}")


class CreateNewPostView(APIView):
    serializer_class = CreateNewPostSerializer

    @authentication.requires_token
    def post(self, request, user_id):
        serializer_result = self.serializer_class(data=request.data)

        if not serializer_result.is_valid():
            return Response(serializer_result.errors)

        posting_user = User.objects.get(id=user_id)
        new_post = Post(content=request.data.get('content'), posted_by=posting_user)
        new_post.save()

        return Response(f"New post added for {posting_user}! Post ID: {new_post.id}")


class LikesInfoView(APIView):
    serializer_class = LikesInfoSerializer

    @staticmethod
    def _has_post_with_zero_likes(user: User) -> bool:
        for post in Post.objects.filter(posted_by=user):
            if post.liked_by.count() == 0:
                return True
        return False

    def _possible_posts_to_like(self, user: User) -> list:
        return [post.id for post in Post.objects.exclude(posted_by=user).exclude(liked_by__in=[user])
                if self._has_post_with_zero_likes(post.posted_by)]

    def _find_next_user(self, users_info: dict, max_num_of_likes: int):
        users_who_can_like = [user for user in users_info if users_info[user]["num_of_likes"] < max_num_of_likes]
        if not users_who_can_like:
            return None

        user_with_max_posts = users_who_can_like[0]
        for user in users_who_can_like:
            if users_info[user]["num_of_posts"] > users_info[user_with_max_posts]["num_of_posts"]:
                user_with_max_posts = user
        return user_with_max_posts

    def post(self, request):
        """ Return the next user to perform a like, and the possible posts to be liked """
        max_num_of_likes = int(request.data.get("max_likes_per_user"))
        users_info = dict()

        for user in User.objects.all():
            users_info[user] = {"num_of_posts": Post.objects.filter(posted_by=user).count(),
                                "num_of_likes": Post.objects.filter(liked_by__in=[user]).count()}

        next_user_to_like = self._find_next_user(users_info, max_num_of_likes)
        if next_user_to_like is None:
            return Response({"user": None, "possible_posts": None})

        possible_posts = self._possible_posts_to_like(next_user_to_like)
        return Response({"user": next_user_to_like.name, "possible_posts": possible_posts})
