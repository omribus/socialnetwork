from app.src.config import Config
from app.src.logger import Logger
import clearbit

clearbit.key = Config.CLEARBIT_API_KEY


def get_additional_data(email: str) -> dict:
    enrichment_result = clearbit.Enrichment.find(email=email)
    Logger.log(f"Enrichment result for '{email}':\n{enrichment_result}")
    return enrichment_result if enrichment_result else {}
