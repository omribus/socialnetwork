from django.contrib import admin
from app.src.models import User, Post

admin.site.register(model for model in [User, Post])
