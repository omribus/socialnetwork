import jwt
import requests
from rest_framework.response import Response
from datetime import datetime, timedelta

from app.src.config import Config
from app.src.models import User
from app.src.logger import Logger


def requires_token(func):
    def wrapper(self, request, user_id=None):
        if user_id:
            return func(self, request, user_id)

        try:
            parsed_token = jwt.decode(request.headers["JWT"], Config.JWT_SECRET, Config.JWT_ALGORITHM)
            Logger.log(f"Valid token from user ID {parsed_token['user_id']}")
            return func(self, request, parsed_token["user_id"])

        except Exception as err:
            return Response(f"Invalid Token - {err}")
    return wrapper


def verify_user_pass(username: str, password: str) -> User:
    return User.objects.get(name=username, password=password)


def create_jwt_token(user: User):
    payload = {"user_id": user.id, "exp": datetime.utcnow() + timedelta(seconds=Config.JWT_EXP_SECONDS)}
    Logger.log(f"Created JWT for user ID: {user.id} (valid for {Config.JWT_EXP_SECONDS} seconds)")
    return jwt.encode(payload, Config.JWT_SECRET, Config.JWT_ALGORITHM)


def verify_email_hunter(email: str) -> str:
    url = f"https://api.hunter.io/v2/email-verifier?email={email}&api_key={Config.HUNTER_API_KEY}"
    result = requests.get(url=url).json()["data"]["status"]

    Logger.log(f"Email verification result for '{email}': {result}")
    return result
