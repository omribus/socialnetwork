from django.contrib import admin
from django.urls import path

from app.src.views.admin_views import *
from app.src.views.api_views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', UserViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('posts/', PostViewSet.as_view({'get': 'list', 'post': 'create'})),

    path('api/signup', CreateNewUserView.as_view()),
    path('api/login', LoginView.as_view()),
    path('api/feed', FeedView.as_view()),
    path('api/new_post', CreateNewPostView.as_view()),
    path('api/likes_info', LikesInfoView.as_view())
]


