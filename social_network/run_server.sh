echo > server/log.txt

HOST=`jq .server_host "server/config.json" | tr -d '"'`
PORT=`jq .server_port "server/config.json"`
python3.6 "server/manage.py" runserver $HOST:$PORT